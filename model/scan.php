<?php
require_once('model/Period.php');
class Scan{
	function scanpassword($password){
		$lower = false;
		$upper = false;
		$specialchar=false;
		$number=false;
		$possibilities = 0;
		$special='!@#$%&*<>-_!@#$%&*<>-_';
		for ($i=0; $i<strlen($password); $i++){
			if (preg_match("/[a-z]/", $password[$i])&&!$lower){
				$possibilities += 26;
				$lower = true;
			}
			if (preg_match('/[A-Z]/',$password[$i])&&!$upper){
				$possibilities += 26;
				$upper = true;
			}
			if (preg_match('/[0-9]/',$password[$i])&&!$number){
				$possibilities += 10;
				$number = true;
			}
			if (strpbrk($password[$i], $special)&&!$specialchar){
				$possibilities += 15;
				$specialchar = true;
			}
		}
		$product['product'] = (pow($possibilities, strlen($password))/4000000000);
		$product['possibilities'] = $possibilities;
		$product['quantity'] = strlen($password);
		$objPeriod = new Period;
		$product['possibilities'] = $objPeriod->mountPossibilities($product);
		return $product;
	}

}