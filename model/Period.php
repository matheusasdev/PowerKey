<?php

class Period{
	function mountPeriod($result){

		if($result<60){	
			$period['value']=$result;
			$period['type']='segundos';
			$period['force']='tooweak';
			$period['level']=0;
		}
		else if($result<3600){
			$period['value']=round(($result/60),2);
			$period['type']='minutos';
			$period['force']='veryweak';
			$period['level']=1;
		}
		else if($result<86400){
			$period['value']=round(($result/3600),2);
			$period['type']='horas';
			$period['force']='weak';
			$period['level']=2;
		}
		else if($result<2592000){
			$period['value']=round(($result/86400),0);
			$period['type']='dias';
			$period['force']='medium';
			$period['level']=3;
		}
		else if($result<31104000){
			$period['value']=round(($result/2592000),2);
			$period['type']='meses';
			$period['force']='medium';
			$period['level']=3;
		}
		else if($result>31104000){
			$result = $result/31104000;
			$indice=0;			
			if($result<1000){
				$period['value']=round($result,2);
				$period['type']= "anos";
				$period['force']='medium';
				$period['level']=3;
				if($result>100){
					$period['force']='strong';
					$period['level']=4;
				}
			}
			else{
					$arr = array("anos", "mil anos", "milhões de anos", "bilhões de anos", "trilhões de anos", "quadrilhões de anos"); 
					while (round($result,0)>1000&&$indice<5	){
						$indice++;
						$result = $result/1000;
						$period['level']=5;
						$period['force']='verystrong';
						$period['value']=round($result,2);						
						$period['type']=$arr[$indice];
					}
				
				if($indice==5||$result>1000){
					unset($period['value']);
					$period['level']=6;
					$period['force']='verystrong';
				}
			}
		}	
		
		return $period;
	}

	function mountPossibilities($product){
		$indice=0;
		$product['possibilities'] = pow($product['possibilities'],$product['quantity']);
		$arr = array('combinações', 'mil combinações', 'milhões de combinações', 'bilhões de combinações.', 'trilhões de combinações.', 'quadrilhões de possibilidades.');
		while(round($product['possibilities'],0)>1000&&$indice<5){
			$indice++;
			$product['possibilities'] = $product['possibilities']/1000;
		}
		if($indice>4){
			$product['possibilities'] = 'Mais de 1 quintilhão de combinações';
		}
		else{$product['possibilities'] = "Aproximadamente ".round($product['possibilities'],0)." ".$arr[$indice];}
		return $product['possibilities'];
	}
}