<!DOCTYPE html>
<html>
    <head>
        <title>Password</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" type="text/css" href="css/style.css">
        <script type="text/javascript" src="scripts/script.js"></script>
    </head>
    <body>
        <header>
            <h1>Power Key</h1>
            <h3>Crie uma senha impossível de ser quebrada</h3>
        </header>


    <section>
      <div id='primaria' class='sections-alinhadas'>
            <?php
            if(isset($mensagens)&&isset($mensagem)){
                echo "<div id='message' class='alerta'><p>";
                echo $mensagem['titulo'];
                for($i=0;$i+1<count($mensagem);$i++){
                    echo $mensagem[$i];}
                echo "</p></div>";
        }?>

        <h4>Quantidade de Caracteres</h4>

            <form id="formulario" method="post" name='formu'>
                <label id="quantidadeCaracteres" for="quantidade">Quantos caracteres deve ter sua senha?</label></br>
                <input id="tamanhonumero" name="tamanho" type="number" value=<?php if(isset($tamanho)){echo $tamanho;}else{echo "6";}?> min="6" max="300" onchange="modificavalor('range')"></br>
                <p id="statussenha">Que tal uma senha um pouco maior?</p>
                <input id="tamanhorange" type="range" value=<?php if(isset($tamanho)){echo $tamanho;}else{echo "6";}?> min="6" max="20" step="1" onchange="modificavalor('numero')"></br>
                
            <img id="imagemavancar" class='botoestransacao' alt="Avançar" title="Avançar" src='images/forward.png' onclick="oculta1()">
        </br>
        <h4>
            <?php
            if($total[0]>10000){
                    echo "Já foram mais de ";
                    echo $total[0];
                    echo " senhas criadas.";
            }
            else{
                echo "Já foram mais de ";
                echo (10160+$total[0]);
                echo " senhas criadas.";
            }
            ?>
        </h4>
      </div>
              <div id='secundaria' class='sections-alinhadas'>

                <h4>Que tipo de caracteres vão compor sua senha?</h4>


                    <table>
                        <tr>
                            <td>
                                <label for="letrasMinusculas">Letras minúsculas:</label>
                            </td>
                            <td>
                                <label for="letrasMaiusculas" >Letras maiúsculas:</label>
                            </td>
                            <td>
                                <label for="numeros">Números:</label>
                            </td>
                            <td>
                                <label for="caracteresEspeciais">Caracteres Especiais:</label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label for="letrasMinusculas"><img  class="imgSelector" src=<?php echo (isset($statusLetrasMinusculas) ? "images/checked.png" : "images/unchecked.png")?> id="imgletrasMinusculas" onClick="changeIcon('letrasMinusculas')"></label>
                                <input id="letrasMinusculas" name="letrasMinusculas" type="checkbox" class="boxOculto" <?php if(isset($statusLetrasMinusculas)){echo "checked";}?>>
                            </td>
                            <td>
                                <label for="letrasMaiusculas"><img class="imgSelector" src=<?php echo (isset($statusLetrasMaiusculas) ? "images/checked.png" : "images/unchecked.png")?> id="imgletrasMaiusculas" onClick="changeIcon('letrasMaiusculas')"></label>
                                <input id="letrasMaiusculas" name="letrasMaiusculas" type="checkbox" class="boxOculto" <?php if(isset($statusLetrasMaiusculas)){echo "checked";}?>>

                            <td>
                                <label for="numeros"><img class="imgSelector" src=<?php echo (isset($statusNumeros) ? "images/checked.png" : "images/unchecked.png")?> id="imgnumeros" onClick="changeIcon('numeros')"></label>
                                <input id="numeros" name="numeros" type="checkbox" class="boxOculto" <?php if(isset($statusNumeros)){echo "checked";}?>>
                            </td>
                            <td><label for="caracteresEspeciais"><img  class="imgSelector" src=<?php echo (isset($statusCaracteresEspeciais) ? "images/checked.png" : "images/unchecked.png")?> id="imgcaracteresEspeciais" onClick="changeIcon('caracteresEspeciais')"></label>
                                <input id="caracteresEspeciais" name="caracteresEspeciais" type="checkbox"class="boxOculto" <?php if(isset($statusCaracteresEspeciais)){echo "checked";}?>>
                            </td>
                        </tr>
                        </table>
                    <div id="divbotoes">
                        <img id='imagemretroceder' alt='Voltar' class='botoestransacao' src='images/backward.png' onclick="oculta2()">                     
                        <input id="botaoConfirma" type="image" src="images/pronto.png">
                    </div> 
                </form>


                      </div>
    </section>
    </body>
</html>
