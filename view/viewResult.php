<!DOCTYPE html>
<html>
    <head>
        <title>Password</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" type="text/css" href="css/style.css">
    </head>
    <body>
        <header>
            <h1>Power Key</h1>
            <h3>Crie uma senha impossível de ser quebrada</h3>
        </header>
    <section>
        <h4>A sua senha é</h4>
        
        <div class='segundalinha'>
            <div id='tableresultado'>
                <div id='iconepowerkey' class='mesmalinha' style='width: 230px;'>
                <p>Clique em mim para criar uma verdadeira PowerKey</p>
                <form method='post'>
                    <input id="powerkey" name="powerkey" type='image' src=<?php echo "images/"; echo $period['force'];?>-padlock.png value='forca'>
                </form>
                </div>
                <div id='textoresultado' class='mesmalinha'>
                    <div class="resultadoSenha"><p><?php echo strval($senha)?></p></div>
                    <p>
                    <?php
                    if(isset($period['value'])){
                        echo "Levaria aproximadamente </br>";
                        echo "<span id='mensagemtempo' class='";
                        echo "level";
                        echo $period['level'];
                        echo "'>";
                        echo $period['value'];
                        echo " ";
                        echo $period['type'];
                        echo "</span>";
                        echo "</br>";
                        echo " para sua senha ser quebrada por um cracker.";}
                    else{
                        echo "Levaria ";
                        echo "<span id='mensagemtempo' class='";
                        echo "level";
                        echo $period['level'];
                        echo "'>";
                        echo "mais de 2 quintilhões de anos ";
                        echo "</span>";
                        echo "para esta senha ser quebrada por um cracker.";
                    }
                    ?>
                </p>
                <p>
                    <?php if(isset($result['possibilities'])){
                            echo "Possibilidades: ";
                            echo $result['possibilities'];
                    }?>
                </p>
                <p>
                    Tentativas por segundo: 4 bilhões
                </p>
            </div></div></div>
        
        <div id="terceiralinha">
            <?php
                $phrases = array("Tá querendo me matar do coração, é? Usar esta senha <span class='bold'>definitivamente não é uma boa idea</span>. Tente colocar mais caracteres e inserir opções de números, letras minúsculas e maiúsculas, além de caracteres especiais.",
                        "Me ajuda aí, amigão. Assim não dá, né?! Senha <span class='bold'>extremamente fraca</span>. Você precisa escolher mais opções.",
                        "Olha só, vai ser difícil manter a amizade assim. <span class='bold'>Sua senha está fraca</span>. Não faça isso com o nosso amiguinho aí do lado.",
                        "Esta senha é de força 'média'. Aquele hacker gordão que não sai da frente do computador tem tempo livre pra quebrá-la. <span class='bold'> Sabemos que você pode fazer melhor que isso.</span>",
                        "A senha está forte. <span class='bold'>Será mais difícil quebrá-la.</span>",
                        "Aeeehooo!!! <span class='bold'>Isso sim é uma senha de verdade!<span> Parabéns!",
                        "Pelos meus cálculos, <span class='bold'>a humanidade será extinta antes que consigam quebrar essa senha.</span>");?>
                <p class=<?php echo 'level'; echo $period['level'];?>>
                    <?php echo $phrases[$period['level']]; ?>
                <p> 


        
        </div>
        <div id='divbotoes'>
            <input type="image" alt="Gerar novamente" class="botoestransacao" src="images/random.png" onclick="location.reload()">
            <input type="image" alt="Voltar" class="botoestransacao" src="images/backward.png" onclick="location.href='index.php'">
        </div>
    </section>

</body>
</html>